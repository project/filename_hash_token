<?php

/**
 * @file
 * Tokens for the Filename Hash Token module.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function filename_hash_token_token_info() {
  $info['tokens']['file']['name-hash'] = [
    'name' => t('File name - hash'),
    'description' => t('A generated 32-character hash of the filename.'),
    'dynamic' => FALSE,
  ];
  $info['tokens']['file']['name-hash-substring'] = [
    'name' => t('File name - hash substring'),
    'description' => t('A substring of characters, based on a specified length and (optional) offset, from a generated 32-character hash of the filename.'),
    'dynamic' => TRUE,
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function filename_hash_token_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $url_options = ['absolute' => TRUE];
  if (isset($language)) {
    $url_options['language'] = $language;
  }

  $replacements = [];

  if ($type == 'file' && !empty($data['file'])) {
    $file_system = \Drupal::service('file_system');

    /** @var \Drupal\file\Entity\File $file */
    $file = $data['file'];

    $basename = $file_system->basename($file->filename->value);

    foreach ($tokens as $name => $original) {
      switch ($name) {
        // A full hash of the file name.
        case 'name-hash':
          $replacements[$original] = md5($basename);
          break;
      }
    }

    if ($hash_substring_tokens = \Drupal::token()->findWithPrefix($tokens, 'name-hash-substring')) {
      foreach ($hash_substring_tokens as $args => $original) {
        // Split the token's arguments into length and offset.
        if (strpos($args, ',') !== FALSE) {
          [$length, $offset] = explode(',', $args);
          if (!is_numeric($offset)) {
            $offset = 0;
          }
        }
        else {
          $length = $args;
          $offset = 0;
        }

        if (is_numeric($length) && $length <= 32) {
          $replacements[$original] = substr(md5($basename), $offset, $length);
        }
      }
    }
  }

  return $replacements;
}
